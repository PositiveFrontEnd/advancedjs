"use strict";
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];
const correctKeys = ["author", "name", "price"];
const wrapper = document.createElement("div");
wrapper.id = "root";
const list = document.createElement("ul");
books.forEach((book, index) => {
  try {
    const li = document.createElement("li");
    correctKeys.forEach((key) => {
      if (!(key in book)) {
        throw new Error(`В ${index} обукті массиві books ключа ${key} немає `);
      }
    });
    for (const key in book) {
      li.append(`${key} : ${book[key]}    `);
    }
    list.append(li);
  } catch (error) {
    console.log(error);
  }
});
wrapper.append(list);
document.body.prepend(wrapper);
document.body.style.textAlign = "center";
