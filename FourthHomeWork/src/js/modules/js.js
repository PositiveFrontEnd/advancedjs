"use strict";
const API = "https://ajax.test-danit.com/api/swapi/films";
const wrapper = document.createElement("div");

function sendRequest(url, method = "GET", options) {
  return fetch(url, { method: method, ...options }).then((response) =>
    response.json()
  );
}

function getListFilm(element) {
  element.classList.add("wrapper");
  document.body.prepend(element);
  sendRequest(`${API}`).then((data) => {
    data.forEach((episode) => {
      const { episodeId, name, openingCrawl, characters } = episode;
      element.insertAdjacentHTML(
        "afterbegin",
        `
            <ul class="wrapper__list list">
              <li class="list__episode">Episode:${episodeId}</li>
              <li class="list__name">Name:${name}</li>
              <li class="list__characters"></li>
              <li class="list__openingCrawl">Summary:${openingCrawl}</li>
            </ul>
          `
      );
      characters.forEach((people) => {
        const liCharacters = document.querySelector(".list__characters");
        liCharacters.innerHTML = "Characters :";
        sendRequest(`${people}`).then((data) => {
          if (!!liCharacters) {
            liCharacters.append(`${data.name} ,`);
          }
        });
        setTimeout(() => {
          liCharacters.style.display = "block";
        }, 2000);
      });
    });
  });
}
getListFilm(wrapper);
