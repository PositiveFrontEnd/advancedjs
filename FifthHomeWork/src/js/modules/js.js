"use strict";
const API = "https://ajax.test-danit.com/api/json";

function sendRequest(url, method = "GET", options) {
  return fetch(url, { method: method, ...options }).then((response) => {
    if (response.ok) {
      if (method === "DELETE") {
        return response;
      }
      return response.json();
    }
  });
}

class Card {
  constructor() {
    this.userData = [];
    this.postData = [];
  }

  async Users() {
    const dataUsers = await sendRequest(`${API}/users`);
    this.userData = dataUsers;
  }

  async Posts() {
    const dataPosts = await sendRequest(`${API}/posts`);
    this.postData = dataPosts;
  }

  render() {
    Promise.all([this.Users(), this.Posts()]).then(() => {
      this.userData.forEach((user) => {
        const { id, email, name } = user;
        const userPosts = this.postData.filter(
          (post) => post.userId === user.id
        );

        userPosts.forEach((post) => {
          const card = document.createElement("div");
          card.classList.add("card");
          card.dataset.id = post.id;
          const cardEmail = document.createElement("p");
          const cardName = document.createElement("p");
          const cardPost = document.createElement("p");
          cardPost.append(post.body);
          const deleteButton = document.createElement("button");
          deleteButton.classList.add("button");
          deleteButton.innerHTML = "Delete";
          cardEmail.append(email);
          cardName.append(name);
          card.append(cardName, cardPost, cardEmail, deleteButton);
          document.body.append(card);
          deleteButton.addEventListener("click", (event) => {
            const deleteTarget = event.target.closest(".button");
            if (deleteTarget) {
              let idTarget = deleteTarget.closest(".card").dataset.id;
              sendRequest(`${API}/posts/${idTarget}`, "DELETE")
                .then((response) => {
                  if (response.ok) {
                    deleteTarget.closest(".card").remove();
                  } else {
                    console.error("Помилка видалення поста:", response.status);
                  }
                })
                .catch((error) => {
                  console.error("Помилка під час видалення поста:", error);
                });
            }
          });
        });
      });
    });
  }
}
const newCard = new Card();
newCard.render();
