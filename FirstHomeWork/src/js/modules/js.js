" use strict ";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, langs) {
    super(name, age, salary);
    this._langs = langs;
  }
  get salary() {
    return this._salary * 3;
  }
  set salary(value) {
    this._salary = value;
  }
}
const employee = new Employee("Yevhen", 34, 1000);
const programmer = new Programmer(
  "Garry",
  43,
  5000,
  "Turkish,English,Ukrainian"
);
const programmerFirst = new Programmer("Igor", 24, 800, "Ukrainian");
const programmerSecond = new Programmer("Gleb", 20, 500, "Turkish");
const programmerThird = new Programmer("Rik", 22, 1500, "English");
console.log(employee);
console.log(programmer);
console.log(programmerFirst);
console.log(programmerSecond);
console.log(programmerThird);
console.log(employee.salary);
console.log(programmer.salary);
console.log(programmerFirst.salary);
console.log(programmerSecond.salary);
console.log(programmerThird.salary);

// Як працює наслідування - ми створюемо якийсь батьківський класс і за допомогою наслідування ми можемо взяти у батьківського классу його властивості ,
// методи і використовувати їх . Взяти властивості можемо тільки від одного классу .
// Для чого потрібо викликати super()- ми через нього передаємо нащі властивості батьківського класу ,
//  для того щоб нащядок їх побачив і міг використовувати
