"use strict";
const button = document.querySelector(".button");
function sendRequest(url, method = "GET", options) {
  return fetch(url, { method: method, ...options }).then((response) => {
    if (response.ok) {
      if (method === "DELETE") {
        return response;
      }
      return response.json();
    }
  });
}

button.addEventListener("click", async (element) => {
  const userApi = await sendRequest("https://api.ipify.org/?format=json").then(
    async (data) => {
      console.log(data);
      const userIp = await sendRequest(
        `http://ip-api.com/json/${data.ip}`
      ).then((id) => {
        const { timezone, country, region, city, regionName } = id;

        document.body.insertAdjacentHTML(
          "afterbegin",
          `
              <div class="wrapper">
                <p class="wrapper__timezone">Continent:${timezone}</p>
                <p class="wrapper__country">Country:${country}</p>
                <p class="wrapper__region">Region:${region}</p>
                <p class="wrapper__city">City:${city}</p>
                <p class="wrapper__regionName">RegionName:${regionName}</p>
              </div>
            `
        );
      });
    }
  );
  button.remove();
});
